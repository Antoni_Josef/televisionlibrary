//
//  SearchVC.swift
//  TelevisionLibrary
//
//  Created by Josef Antoni on 11.10.17.
//  Copyright © 2017 Josef Antoni. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift
import SwiftLoader
import UserNotifications

class SearchVC: ImprovedViewController {
    
    fileprivate var _tvShowResults: MovieResults?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self._requestForNewToken()
        self._setupView()
        self._customCellRegister()
    }
    
    /*
     *  Register nib view for tableView.
     */
    fileprivate func _customCellRegister(){
        let nib = UINib(nibName: "CustomTableCellView", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "CustomTableCell")
        tableView.separatorStyle = .none
    }
    
    /*
     *  Show View of collection with user saved tv shows.
     */
    fileprivate func _openCollectionVC(){
        let openNewVC = self.storyboard?.instantiateViewController(withIdentifier: "CollectionVC") as! CollectionVC
        DispatchQueue.main.async(execute: { () -> Void in
            self.navigationController?.pushViewController(openNewVC, animated: true)
        });
    }
    
    /*
     *  Get new token for use with REST API requests.
     */
    fileprivate func _requestForNewToken(){
        if isConnectedToNetwork() {
            SwiftLoader.show(animated: true)
            AuthRequests().getToken { (done) in
                SwiftLoader.hide()
            }
        } else {
            self.showSimpleAlert(title: "ERR", message: "ERR_NO_INTERNET")
        }
    }
    
    /*
     *  Search for show by keyword typed in textfield.
     */
    fileprivate func _searchForShow(keyword: String, page: String){
        self.view.endEditing(true)
        if isConnectedToNetwork() {
            if !keyword.isEmpty {
                SwiftLoader.show(animated: true)
                SearchRequests().searchForMovie(keyword: keyword, page: page) { (done, results) in
                    SwiftLoader.hide()
                    if done {
                        self._tvShowResults = results
                        self.tableView.reloadData()
                    } else {
                        self.showSimpleAlert(title: "ERR", message: "ERR_RESULT")
                    }
                }
            } else {
                self.showSimpleAlert(title: "ERR", message: "NO_KEYWORD")
            }
        } else {
            self.showSimpleAlert(title: "ERR", message: "ERR_NO_INTERNET")
        }
    }
    
    /*
     *  Show detail of tv show
     */
    fileprivate func _openDetailOfShow(index: Int){
        let openNewVC = self.storyboard?.instantiateViewController(withIdentifier: "DetailOfShowVC") as! DetailOfShowVC
        openNewVC.detailData = DetailData(id: _tvShowResults!.idArr[index], name: _tvShowResults!.nameArr[index], description: _tvShowResults!.descriptionArr[index], backgroundImage: _tvShowResults!.backgroundImageArr[index])
        DispatchQueue.main.async(execute: { () -> Void in
            self.navigationController?.pushViewController(openNewVC, animated: true)
        });
    }
    
    fileprivate func _setupView(){
        searchField.placeholder = NSLocalizedString("SEARCHFIELD_NAME_OF_SHOW", comment: "")
        searchContainer.layer.borderWidth = 1
        searchContainer.layer.borderColor = UIColor.gray.cgColor
        searchContainer.layer.cornerRadius = searchField.frame.height/2
        searchField.delegate = self
    }
    
    @IBOutlet var tableView: UITableView!
    @IBOutlet var searchField: UITextField!
    @IBOutlet var searchContainer: UIView!
    @IBOutlet var seaarchBtn: UIButton!
    @IBOutlet var myCollectionBtn: UIBarButtonItem!
}

extension SearchVC {
    
    @IBAction func searchBtn(_ sender: Any) {
        _searchForShow(keyword: searchField.text!, page: "1")
    }
    
    @IBAction func collectionBtn(_ sender: Any) {
        self.view.endEditing(true)
        _openCollectionVC()
    }
}

extension SearchVC: UITextFieldDelegate {

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        _searchForShow(keyword: searchField.text!, page: "1")
        return true
    }
    
}

extension SearchVC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if _tvShowResults != nil {
            return _tvShowResults!.nameArr.count
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell     {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "CustomTableCell") as! CustomTableCellVC
        cell.promoName.text = _tvShowResults!.nameArr[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.view.endEditing(true)
        self._openDetailOfShow(index: indexPath.row)
        self.tableView.deselectRow(at: indexPath, animated: false)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.view.endEditing(true)
    }
}
