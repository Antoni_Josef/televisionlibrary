//
//  EpisodeTableVC.swift
//  TelevisionLibrary
//
//  Created by Josef Antoni on 13.10.17.
//  Copyright © 2017 Josef Antoni. All rights reserved.
//

import Foundation
import UIKit
import SwiftLoader
import RealmSwift

class EpisodeTableVC: ImprovedViewController {
    
    var episodeData: ShowSeasonData?
    fileprivate var _showEpisodeData: ShowEpisodeData?
    fileprivate var _previouslySelectedHeaderIndex: Int?
    fileprivate var _selectedHeaderIndex: Int?
    fileprivate var _selectedItemIndex: Int?
    fileprivate let _cells = SwiftyAccordionCells()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        _customCellRegister()
        _insertEpisodesWithShowToList()
    }
    
    /*
     *  Show tablecells with seasons and episodes.
     */
    fileprivate func _insertEpisodesWithShowToList(){
        if let epData = episodeData {
            for i in 0..<epData.numberOfEpisodesArr.count {
                self._cells.append(SwiftyAccordionCells.HeaderItem(value: epData.seasonNumberArr[i]))
                if let number: Int = Int(epData.numberOfEpisodesArr[i]) {
                    if 1 <= number {
                        let ep = Array(1...number)
                        for j in 0..<ep.count {
                            self._cells.append(SwiftyAccordionCells.Item(value: ep[j].description))
                        }
                    }
                }
            }
            tableView.reloadData()
        }
    }
    
    fileprivate func getEpisodeData(episodeNumber: String){
        SwiftLoader.show(animated: true)
        SearchRequests().searchForShowEpisodes(showId: episodeData!.showId, seasonNumber: episodeNumber) { (done, showEpisodeData) in
            SwiftLoader.hide()
            self._showEpisodeData = showEpisodeData
        }
    }
    
    fileprivate func _customCellRegister(){
        let nib = UINib(nibName: "CustomTableCellView", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "CustomTableCell")
        tableView.separatorStyle = .none
    }
    
    /*
     *  Check if there is notification for episode in db and if not execute new timer for alert.
     */
    fileprivate func _scheduleNotifications(dateOfRelease: Date, episode: String) {
        let realm = try! Realm()
        let results = realm.objects(EpisodeAlertModel.self).filter("alertedDate >= %@", Date()).sorted(byKeyPath: "alertedDate", ascending: false)
        if let ep = Int(episode) {
            print(_showEpisodeData!.episodeIdArr[ep-1])
            let showActiveInCounting = realm.objects(EpisodeAlertModel.self).filter("alertedDate >= %@ AND episodeId == '\(_showEpisodeData!.episodeIdArr[ep-1])'", Date()).sorted(byKeyPath: "alertedDate", ascending: false)
            print(showActiveInCounting)
            print(results)
            if showActiveInCounting.count == 0 {
                if results.count < 64 {
                    let episode = EpisodeAlertModel()
                    episode.episodeId = _showEpisodeData!.episodeIdArr[ep-1]
                    episode.alertedDate = dateOfRelease
                    
                    let realm = try! Realm()
                    try! realm.write {
                        realm.add(episode)
                    }
                    let notification = UILocalNotification()
                    notification.fireDate = dateOfRelease//Date().addingTimeInterval(TimeInterval(10))
                    notification.alertBody = title!
                    notification.alertAction = NSLocalizedString("EP_RDY", comment: "")
                    notification.hasAction = true
                    UIApplication.shared.scheduleLocalNotification(notification)
                    self.showSimpleAlert(title: "DONE", message: "ALERT_ACTIVE")
                } else {
                    self.showSimpleAlert(title: "ERR", message: "ALERT_FULL")
                }
            } else {
                self.showSimpleAlert(title: "ERR", message: "ALERT_ALREADY_IN")
            }
        }
    }
    
    @IBOutlet var tableView: UITableView!
}

extension EpisodeTableVC {
    
    @IBAction func backBtn(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}

extension EpisodeTableVC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self._cells.items.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if _cells.items.isEmpty == false {
            let item = self._cells.items[indexPath.row]
            if item is SwiftyAccordionCells.HeaderItem {
                return 100
            } else if (item.isHidden) {
                return 0
            } else {
                return 70
            }
        }
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell     {
        if _cells.items.isEmpty == false {
            let item = self._cells.items[indexPath.row]
            let value = item.value
            if item is SwiftyAccordionCells.HeaderItem {
                let cell = self.tableView.dequeueReusableCell(withIdentifier: "CustomTableCell") as! CustomTableCellVC
                cell.promoName.text = NSLocalizedString("SEASON_CELL_TITLE", comment: "") + value
                cell.promoName.textAlignment = .center
                cell.backgroundColor = UIColor.gray
                return cell
                
            } else {
                let cell = self.tableView.dequeueReusableCell(withIdentifier: "CustomTableCell") as! CustomTableCellVC
                cell.promoName.text = NSLocalizedString("EP_CELL_TITLE", comment: "") + value
                cell.promoName.textAlignment = .center
                cell.backgroundColor = UIColor.lightGray
                return cell
            }
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = self._cells.items[indexPath.row]
        
        if item is SwiftyAccordionCells.HeaderItem {
            if self._selectedHeaderIndex == nil {
                self._selectedHeaderIndex = indexPath.row
            } else {
                self._previouslySelectedHeaderIndex = self._selectedHeaderIndex
                self._selectedHeaderIndex = indexPath.row
            }
            
            if let previouslySelectedHeaderIndex = self._previouslySelectedHeaderIndex {
                self._cells.collapse(previouslySelectedHeaderIndex)
            }
            
            if self._previouslySelectedHeaderIndex != self._selectedHeaderIndex {
                self._cells.expand(self._selectedHeaderIndex!)
                self.tableView.beginUpdates()
                self.tableView.endUpdates()
                getEpisodeData(episodeNumber: item.value)
            } else {
                self._selectedHeaderIndex = nil
                self._previouslySelectedHeaderIndex = nil
            }
            
            self.tableView.beginUpdates()
            self.tableView.endUpdates()
        } else {
            self._selectedHeaderIndex = indexPath.row
            let now = Date()
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            let value: String = item.value
            if let index = Int(value) {
                if let futureDate = dateFormatter.date(from: _showEpisodeData!.airDateArr[index-1]) {
                    if  futureDate > now {
                        _scheduleNotifications(dateOfRelease: futureDate, episode: item.value)
                    }
                }
            }
        }
    }
    
}
