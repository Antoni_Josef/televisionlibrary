//
//  CollectionVC.swift
//  TelevisionLibrary
//
//  Created by Josef Antoni on 12.10.17.
//  Copyright © 2017 Josef Antoni. All rights reserved.
//

import Foundation
import UIKit
import SDWebImage
import RealmSwift
import SwiftLoader

class CollectionVC: ImprovedViewController {
    
    fileprivate var _tvShowCollectionResults: Results<TvShowModel>?

    override func viewDidLoad() {
        super.viewDidLoad()
        _customCellRegister()
        _setupScene()
    }
    
    fileprivate func _setupScene(){
        self.title = NSLocalizedString("MY_COLLECTION_NAVBAR_TITLE", comment: "")
        _loadResults()
    }
    
    fileprivate func _loadResults(){
        //show last one added as 1st
        let realm = try! Realm()
        _tvShowCollectionResults = realm.objects(TvShowModel.self).sorted(byKeyPath: "dateOfAddToCollection", ascending: false)
    }
    
    /*
     *  Get tv seasons acording to tv show id saved in collection
     */
    fileprivate func _getShowSeasons(showId: String, showName: String){
        if isConnectedToNetwork() {
            SwiftLoader.show(animated: true)
            SearchRequests().searchForShowSeasons(showId: showId, completionHandler: { (done, showSeasonsData) in
                SwiftLoader.hide()
                if done {
                    let modal = self.storyboard?.instantiateViewController(withIdentifier: "EpisodeTableVC") as! EpisodeTableVC
                    modal.episodeData = showSeasonsData
                    modal.title = showName
                    let navigationController = UINavigationController(rootViewController: modal)
                    navigationController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                    self.present(navigationController, animated: true, completion: nil)
                } else {
                    self.showSimpleAlert(title: "ERR", message: "ERR_SERVER_SIDE")
                }
            })
        } else {
            self.showSimpleAlert(title: "ERR", message: "ERR_NO_INTERNET")
        }
    }
    
    fileprivate func _customCellRegister(){
        let nib = UINib(nibName: "CollectionCellView", bundle: nil)
        collectionView.register(nib, forCellWithReuseIdentifier: "CollectionCell")
    }
    
    @IBOutlet var collectionView: UICollectionView!
}

extension CollectionVC {
    
    @IBAction func backBtn(_ sender: Any) {
        if let navigationController = self.navigationController {
            navigationController.popViewController(animated: true)
        }
    }
}

extension CollectionVC: UICollectionViewDelegate, UICollectionViewDataSource {
    
    /*
     *  Return number of cells and adjust spacing between them.
     */
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            let size = (self.view.frame.width/3)-3
            layout.itemSize = CGSize(width: size, height: size)
            layout.minimumLineSpacing = 3
            layout.minimumInteritemSpacing = 3
            layout.invalidateLayout()
        }
        if _tvShowCollectionResults == nil {
            return 0
        } else {
            return _tvShowCollectionResults!.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionCell", for: indexPath) as! CollectionCellVC
        cell.titleLabel.text! = _tvShowCollectionResults![indexPath.row].nameOfTitle
        let imagePath = _tvShowCollectionResults![indexPath.row].imageOfTitle
        let placeholder = UIImage(named: "placeholder")
        cell.backGroundImage.sd_setImage(with: URL(string: "https://image.tmdb.org/t/p/w300\(imagePath)"), placeholderImage: placeholder)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        _getShowSeasons(showId: _tvShowCollectionResults![indexPath.row].id, showName: _tvShowCollectionResults![indexPath.row].nameOfTitle)
    }
}
