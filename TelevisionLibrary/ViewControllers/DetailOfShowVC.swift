//
//  DetailOfShowVC.swift
//  TelevisionLibrary
//
//  Created by Josef Antoni on 11.10.17.
//  Copyright © 2017 Josef Antoni. All rights reserved.
//

import Foundation
import UIKit
import SDWebImage
import RealmSwift

struct DetailData {
    var id, name, description, backgroundImage: String
}

class DetailOfShowVC: UIViewController {

    var detailData: DetailData?
    fileprivate let _realm = try! Realm()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        _setupScene()
    }
    
    fileprivate func _setupScene(){
        title = detailData?.name
        self.descTxtView.text = detailData?.description
        self.backgoundImage.sd_setImage(with: URL(string: "https://image.tmdb.org/t/p/w300\(detailData!.backgroundImage)"), placeholderImage: UIImage(named: "placeholder"))
        if _checkIfValueWillBeUnique().isEmpty {
            self.addBtn.setTitle(NSLocalizedString("ADD_SHOW_TO_COLLECTION", comment: ""), for: .normal)
        } else {
            self.addBtn.setTitle(NSLocalizedString("DELETE_SHOW_FROM_COLLECTION", comment: ""), for: .normal)
        }
    }
    
    /*
     *  Check if there is tv show inside collection, if there is not addit. Else remove it.
     */
    fileprivate func _addShowToList(){
        let newShow = TvShowModel()
        newShow.id = detailData!.id
        newShow.nameOfTitle = detailData!.name
        newShow.imageOfTitle = detailData!.backgroundImage
        
        let result = _checkIfValueWillBeUnique()
        if result.isEmpty {
            try! _realm.write {
                _realm.add(newShow)
            }
            self.addBtn.setTitle(NSLocalizedString("DELETE_SHOW_FROM_COLLECTION", comment: ""), for: .normal)
        } else {
            try! _realm.write {
                _realm.delete(result)
            }
            self.addBtn.setTitle(NSLocalizedString("ADD_SHOW_TO_COLLECTION", comment: ""), for: .normal)
        }
    }
    
    fileprivate func _checkIfValueWillBeUnique() -> Results<TvShowModel> {
        let result = _realm.objects(TvShowModel.self).filter("id = '\(detailData!.id)'")
        return result
    }

    @IBOutlet var addBtn: UIButton!
    @IBOutlet var backgoundImage: UIImageView!
    @IBOutlet var descTxtView: UITextView!
}

extension DetailOfShowVC {
    
    @IBAction func addBtn(_ sender: Any) {
        _addShowToList()
    }
    
    @IBAction func backBtn(_ sender: Any) {
        if let navigationController = self.navigationController {
            navigationController.popViewController(animated: true)
        }
    }
}
