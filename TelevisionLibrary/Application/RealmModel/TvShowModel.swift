//
//  TvShowModel.swift
//  TelevisionLibrary
//
//  Created by Josef Antoni on 12.10.17.
//  Copyright © 2017 Josef Antoni. All rights reserved.
//

import Foundation
import RealmSwift

class TvShowModel: Object {
    @objc dynamic var id: String = ""
    @objc dynamic var nameOfTitle: String = ""
    @objc dynamic var imageOfTitle: String = ""
    @objc dynamic var dateOfAddToCollection: Date = Date()
}
