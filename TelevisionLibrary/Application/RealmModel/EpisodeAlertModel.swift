//
//  EpisodeAlertModel.swift
//  TelevisionLibrary
//
//  Created by Josef Antoni on 20.10.17.
//  Copyright © 2017 Josef Antoni. All rights reserved.
//

import Foundation
import RealmSwift

class EpisodeAlertModel: Object {
    @objc dynamic var episodeId: String = ""
    @objc dynamic var alertedDate: Date = Date()
}
