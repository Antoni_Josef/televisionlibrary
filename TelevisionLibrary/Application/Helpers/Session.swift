//
//  Session.swift
//  TelevisionLibrary
//
//  Created by Josef Antoni on 11.10.17.
//  Copyright © 2017 Josef Antoni. All rights reserved.
//

import Foundation

class Session {
    
    static let sharedInstance = Session()
    fileprivate init(){//This prevents others from using the default '()' initializer for this class.
    }
    let apiKey: String = "6aa0330a9d15f7ef303279b47028c763"
    let urlDomain: String = "https://api.themoviedb.org/3"
    var userToken: String = ""
}
