//
//  AuthRequests.swift
//  TelevisionLibrary
//
//  Created by Josef Antoni on 11.10.17.
//  Copyright © 2017 Josef Antoni. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class AuthRequests {
    
    fileprivate let s = Session.sharedInstance
    
    //Create a temporary request token that can be used to validate a TMDb user login.
    func getToken(completionHandler: @escaping (Bool) -> ()) -> (){
        
        let url = "\(self.s.urlDomain)/authentication/token/new?api_key=\(self.s.apiKey)"
        
        Alamofire.request(url, method: .get, encoding: JSONEncoding.default).responseJSON { response in
            //print(response.debugDescription)
            if response.response?.statusCode == 200 {
                if let data = response.result.value {
                    let json = JSON(data)
                    let success = json["success"].boolValue
                    if success {
                        //let expires = json["expires_at"].stringValue
                        let token = json["request_token"].stringValue
                        self.s.userToken = token
                        completionHandler(true)
                    } else {
                        completionHandler(false)
                    }
                } else {
                    completionHandler(false)
                }
            } else {
                completionHandler(false)
            }
        }
    }
}
