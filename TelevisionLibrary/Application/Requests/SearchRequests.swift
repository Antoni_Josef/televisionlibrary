//
//  SearchRequests.swift
//  TelevisionLibrary
//
//  Created by Josef Antoni on 11.10.17.
//  Copyright © 2017 Josef Antoni. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

struct MovieResults {
    var idArr, nameArr, descriptionArr, backgroundImageArr: [String]
    
    init(idArr: [String], nameArr: [String], descriptionArr: [String], backgroundImageArr: [String]) {
        self.idArr = idArr
        self.nameArr = nameArr
        self.descriptionArr = descriptionArr
        self.backgroundImageArr = backgroundImageArr
    }
}

struct ShowSeasonData {
    var showId: String
    var seasonNumberArr, numberOfEpisodesArr: [String]
    
    init(showId: String, seasonNumberArr: [String], numberOfEpisodesArr: [String]) {
        self.showId = showId
        self.seasonNumberArr = seasonNumberArr
        self.numberOfEpisodesArr = numberOfEpisodesArr
    }
}

struct ShowEpisodeData {
    var episodeIdArr, airDateArr, episodeNumberArr, posterPathArr: [String]
    
    init(episodeIdArr: [String], episodeNumberArr: [String], airDateArr: [String], posterPathArr: [String]) {
        self.episodeIdArr = episodeIdArr
        self.episodeNumberArr = episodeNumberArr
        self.airDateArr = airDateArr
        self.posterPathArr = posterPathArr
    }
}

class SearchRequests {
    
    fileprivate let s = Session.sharedInstance
    
    //Create a temporary request token that can be used to validate a TMDb user login.
    func searchForMovie(keyword: String, page: String, completionHandler: @escaping (Bool, MovieResults?) -> ()) -> (){
        var url: String = ""
        if let keyword = keyword.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) {
            url = "\(self.s.urlDomain)/search/tv?api_key=\(self.s.apiKey)&language=en-US&query=\(keyword)&page=\(page)"
            if let lang = NSLocale.current.languageCode {
                if lang == "cs" {
                    url = url + "&language=cs"
                }
            }
        }
        
        Alamofire.request(url, method: .get, encoding: JSONEncoding.default).responseJSON { response in
            //print(response.debugDescription)
            if response.response?.statusCode == 200 {
                if let data = response.result.value {
                    let json = JSON(data)
                    
                    var idArr: [String] = []
                    var nameArr: [String] = []
                    var descriptionArr: [String] = []
                    var backgroundImageArr: [String] = []
                    
                    for i in 0..<json["results"].count {
                        let id = json["results"][i]["id"].stringValue
                        let name = json["results"][i]["name"].stringValue
                        let backgroundImage = json["results"][i]["backdrop_path"].stringValue
                        let overview = json["results"][i]["overview"].stringValue
                        
                        idArr.append(id)
                        nameArr.append(name)
                        descriptionArr.append(overview)
                        backgroundImageArr.append(backgroundImage)
                    }
                    let results: MovieResults = MovieResults(idArr: idArr, nameArr: nameArr, descriptionArr: descriptionArr, backgroundImageArr: backgroundImageArr)
                    completionHandler(true, results)
                } else {
                    completionHandler(false, nil)
                }
            } else {
                completionHandler(false, nil)
            }
        }
    }
    
    func searchForShowSeasons(showId: String, completionHandler: @escaping (Bool, ShowSeasonData?) -> ()) -> (){
        
        let url = "\(self.s.urlDomain)/tv/\(showId)?api_key=\(self.s.apiKey)"
        
        Alamofire.request(url, method: .get, encoding: JSONEncoding.default).responseJSON { response in
            print(response.debugDescription)
            if response.response?.statusCode == 200 {
                if let data = response.result.value {
                    let json = JSON(data)
                    var seasonNumberArr: [String] = []
                    var numberOfEpisodesArr: [String] = []
                    for i in 0..<json["seasons"].count {
                        let seasonNumber = json["seasons"][i]["season_number"].stringValue
                        let numberOfEpisodes = json["seasons"][i]["episode_count"].stringValue
                        
                        seasonNumberArr.append(seasonNumber)
                        numberOfEpisodesArr.append(numberOfEpisodes)
                    }
                    let showSeasonsData: ShowSeasonData = ShowSeasonData(showId: showId, seasonNumberArr: seasonNumberArr, numberOfEpisodesArr: numberOfEpisodesArr)
                    completionHandler(true, showSeasonsData)
                } else {
                    completionHandler(false, nil)
                }
            } else {
                completionHandler(false, nil)
            }
        }
    }
    
    func searchForShowEpisodes(showId: String, seasonNumber: String, completionHandler: @escaping (Bool, ShowEpisodeData?) -> ()) -> (){
        
        let url = "\(self.s.urlDomain)/tv/\(showId)/season/\(seasonNumber)?api_key=\(self.s.apiKey)"
        
        Alamofire.request(url, method: .get, encoding: JSONEncoding.default).responseJSON { response in
            print(response.debugDescription)
            if response.response?.statusCode == 200 {
                if let data = response.result.value {
                    let json = JSON(data)
                    var episodeIdArr: [String] = []
                    var episodeNumberArr: [String] = []
                    var airDateArr: [String] = []
                    var posterPathArr: [String] = []
                    
                    for i in 0..<json["episodes"].count {
                        let episodeId = json["episodes"][i]["id"].stringValue
                        let episodeNumber = json["episodes"][i]["episode_number"].stringValue
                        let airDate = json["episodes"][i]["air_date"].stringValue//Outlander
                        let posterPath = json["episodes"][i]["still_path"].stringValue
                        
                        episodeIdArr.append(episodeId)
                        episodeNumberArr.append(episodeNumber)
                        posterPathArr.append(posterPath)
                        airDateArr.append(airDate)
                    }
                    let showEpisodeData: ShowEpisodeData = ShowEpisodeData(episodeIdArr: episodeIdArr, episodeNumberArr: episodeNumberArr, airDateArr: airDateArr, posterPathArr: posterPathArr)
                    completionHandler(true, showEpisodeData)
                } else {
                    completionHandler(false, nil)
                }
            } else {
                completionHandler(false, nil)
            }
        }
    }
    
}
