//
//  CustomTableCellVC.swift
//  TelevisionLibrary
//
//  Created by Josef Antoni on 11.10.17.
//  Copyright © 2017 Josef Antoni. All rights reserved.
//

import Foundation
import UIKit

class CustomTableCellVC: UITableViewCell {
    
    override func awakeFromNib() {
        selectionStyle = UITableViewCellSelectionStyle.none
    }
    
    @IBOutlet var promoName: UILabel!
    
}
