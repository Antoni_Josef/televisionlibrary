//
//  CollectionCellVC.swift
//  TelevisionLibrary
//
//  Created by Josef Antoni on 13.10.17.
//  Copyright © 2017 Josef Antoni. All rights reserved.
//

import Foundation
import UIKit

class CollectionCellVC: UICollectionViewCell {
    
    override func awakeFromNib() {
        self.backGroundImage.clipsToBounds = true
    }
    
    @IBOutlet var backGroundImage: UIImageView!
    @IBOutlet var titleLabel: UILabel!
    
}
